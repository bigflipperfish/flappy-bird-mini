from time import sleep
import pygame
import sys
import random

# ------------------------------------------------------------------------------
# Define the resource paths that will be used in-game
# ------------------------------------------------------------------------------
PATH = 'resource/'
BG_DAY = f'{PATH}background-day.png'
BG_NIGHT = f'{PATH}background-night.png'
FLOOR = f'{PATH}floor.png'
GAME_OVER = f'{PATH}gameover.png'
START = f'{PATH}start.png'
PIPE_GN = f'{PATH}pipe-green.png'
PIPE_RD = f'{PATH}pipe-red.png'
BIRD = f'{PATH}bird_mid.png'  # Replace the 'mid' with 'up', 'mid', or 'down'
SCOREBOARD = f'{PATH}scoreboard.png'
FONT = f'{PATH}Font/flappy-font.ttf'

# ------------------------------------------------------------------------------
# Define the height and width of the screen
# ------------------------------------------------------------------------------
WIDTH = 128
HEIGHT = 128

# ------------------------------------------------------------------------------
# Define game events
# ------------------------------------------------------------------------------
SPAWN_PIPE = pygame.USEREVENT

# ------------------------------------------------------------------------------
# Initialize pygame parameters.
# ------------------------------------------------------------------------------
pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
game_clock = pygame.time.Clock()

# ------------------------------------------------------------------------------
# Define game variables
# ------------------------------------------------------------------------------
FPS = 60
GRAVITY = 1
GAP_SIZE = 15
PIPE_SPEED = 0.75
PIPE_SPAWNTIME = 1800
SCORE_FONT = pygame.font.Font(FONT, 20)
DRAW_HITBOX = False


# ------------------------------------------------------------------------------
# Create a class for in-game sprites.
#
# ARGUMENTS:
# path: The path to the sprite png file.
# x_pos: The initial x position of the sprite (Defaults to 0).
# y_pos: The initial y position of the sprite (Defaults to 0).
# scroll_speed: The speed at which the sprite will scroll across the screen
# ------------------------------------------------------------------------------
class Sprite:
    def __init__(self, path, x_pos=0, y_pos=0, scroll_speed=0.0):
        self.img = pygame.image.load(path)
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.scroll_speed = scroll_speed

    # --------------------------------------------------------------------------
    # Scroll the sprite across the screen on the x axis. This method creates as
    # many copies of the sprite as needed to span the entire screen.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def scroll(self):
        copies_needed = WIDTH // self.img.get_width() + 2
        copies = []

        for num in range(copies_needed):
            copies.append(self.img.copy())
        screen.blit(self.img, (self.x_pos, self.y_pos))

        for num, img in enumerate(copies):
            screen.blit(img, (self.x_pos + img.get_width() * num, self.y_pos))

        self.x_pos -= self.scroll_speed
        if self.x_pos < -self.img.get_width():
            self.x_pos = 0

    # --------------------------------------------------------------------------
    # Freeze the position of a scrolling sprite.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def freeze(self):
        copies_needed = WIDTH // self.img.get_width() + 2
        copies = []

        for num in range(copies_needed):
            copies.append(self.img.copy())
        screen.blit(self.img, (self.x_pos, self.y_pos))

        for num, img in enumerate(copies):
            screen.blit(img, (self.x_pos + img.get_width() * num, self.y_pos))

    # --------------------------------------------------------------------------
    # Draw the sprite on the screen centered on the provided x and y position.
    #
    # ARGUMENTS:
    # x_pos: The position on the x axis to center the sprite
    # y_pos: The position on the y axis to center the sprite
    # --------------------------------------------------------------------------
    def draw(self):
        screen.blit(self.img, (self.x_pos, self.y_pos))


# ------------------------------------------------------------------------------
# Create a class for in-game entities.
#
# ARGUMENTS:
# flipped: True if entity is flipped horizontally
# path: The path to the sprite png file.
# x_pos: The initial x position of the entity (Defaults to 0).
# y_pos: The initial y position of the entity (Defaults to 0).
# weight: The amount that gravity affects the entity (Defaults to 0).
# jump_power: How strong the entity's ability to jump is (Defaults to 0).
# scroll_speed: How fast the entity scrolls (Defaults to 0)
# ------------------------------------------------------------------------------
class Entity:
    def __init__(self, path, x_pos=0.0, y_pos=0.0):
        self.path = path
        self.img = pygame.image.load(self.path)
        self.center = (self.img.get_height() // 2, self.img.get_width() // 2)
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.hit_box = self.img.get_rect(center=(self.x_pos, self.y_pos))

    # --------------------------------------------------------------------------
    # Draw the entity at the correct location on the screen
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def draw(self):
        self.hit_box.centery = self.y_pos
        screen.blit(self.img, self.hit_box)
        if DRAW_HITBOX:
            pygame.draw.rect(screen, (255, 0, 0), self.hit_box, 2)

    # --------------------------------------------------------------------------
    # Detect a collision the entity and an entity passed in as a variable.
    #
    # ARGUMENTS:
    # entity: the entity to check for a collision with.
    # --------------------------------------------------------------------------
    def detect_collision(self, entity):
        # Check for collision on the x axis
        if self.hit_box[0] >= entity.hit_box[0] - (entity.hit_box[2] // 2):
            if self.hit_box[0] <= entity.hit_box[0] + (entity.hit_box[2] // 2):
                # Check for a collision on the y axis
                if self.hit_box.top <= entity.hit_box.bottom:
                    if self.hit_box.bottom >= entity.hit_box.top:
                        return True


# ------------------------------------------------------------------------------
# The bird flaps and flies through gaps in pipes.
#
# ARGUMENTS:
# None
# ------------------------------------------------------------------------------
class Bird(Entity):
    def __init__(self, path, x_pos, y_pos, weight=0.0, jump_power=0.0):
        super().__init__(path, x_pos, y_pos)
        self.flap_path = ''
        self.weight = weight * GRAVITY
        self.jump_power = jump_power
        self.momentum = 0
        self.rotation = 0
        self.min_y, self.max_y = 5, 105

    # --------------------------------------------------------------------------
    # Set the bird image with the proper flap state by replacing 'mid' in the
    # image's path.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def set_flap_state(self):
        if self.momentum < 0:
            state = 'down'
        elif self.momentum == 0:
            state = 'mid'
        else:
            state = 'up'
        self.flap_path = (self.path.replace('mid', state))
        self.img = pygame.image.load(self.flap_path)

    # --------------------------------------------------------------------------
    # Rotate the bird before it is drawn.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def rotate_bird(self):
        self.set_flap_state()
        self.rotation = -self.momentum * 5
        bird_rotated = pygame.transform.rotate(self.img, self.rotation)
        return bird_rotated

    # --------------------------------------------------------------------------
    # Draw the entity at the correct location on the screen after it has been
    # affected by gravity, then move the hit box to match the location.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def draw(self):
        self.momentum += self.weight
        self.rotation = -self.momentum * 6
        self.y_pos += self.weight + self.momentum
        self.hit_box.centery = self.y_pos
        screen.blit(self.rotate_bird(), self.hit_box)
        if self.y_pos > self.max_y:
            self.y_pos = self.max_y
            self.momentum = 0
        elif self.y_pos < self.min_y:
            self.y_pos = self.min_y
            self.momentum = 0
        if DRAW_HITBOX:
            pygame.draw.rect(screen, (255, 0, 0), self.hit_box, 2)

    # --------------------------------------------------------------------------
    # Make the bird jump. FLY BIRD, FLY!
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def jump(self):
        self.momentum = -self.weight * self.jump_power


# ------------------------------------------------------------------------------
# The pipes are obstacles for the bird to fly through.
#
# ARGUMENTS:
# None
# ------------------------------------------------------------------------------
class Pipe(Entity):
    def __init__(self, path, x_pos, y_pos, scroll_speed=0.0, flipped=False):
        super().__init__(path, x_pos, y_pos)
        if flipped:
            self.img = pygame.transform.flip(self.img, False, True)
        self.scroll_speed = scroll_speed

    # --------------------------------------------------------------------------
    # Draw the entity at the correct location on the screen after it has been
    # scrolled, then move the hit box to match the location.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def scroll(self):
        self.x_pos -= self.scroll_speed
        self.hit_box.centerx = self.x_pos
        screen.blit(self.img, self.hit_box)
        if DRAW_HITBOX:
            pygame.draw.rect(screen, (255, 0, 0), self.hit_box, 2)


# ------------------------------------------------------------------------------
# Create a class to store pipe entities in.
#
# ARGUMENTS:
# None
# ------------------------------------------------------------------------------
class PipeList:
    def __init__(self, img):
        self.pipes = []
        self.pipe_img = img

    # --------------------------------------------------------------------------
    # Spawn pipes into the pipes list leaving gaps at random heights on the
    # screen.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def spawn_pipe(self):
        y_pos = (random.randint(75, 140))

        bottom_pipe = Pipe(self.pipe_img, WIDTH + 10, y_pos + GAP_SIZE,
                           scroll_speed=PIPE_SPEED, flipped=False)

        top_pipe = Pipe(self.pipe_img, WIDTH + 10, y_pos - 100 - GAP_SIZE,
                        scroll_speed=PIPE_SPEED, flipped=True)

        self.pipes.append(bottom_pipe)
        self.pipes.append(top_pipe)

    # --------------------------------------------------------------------------
    # Scroll the pipes across the screen and remove them from the list once they
    # are no longer visible.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def scroll_pipes(self):
        for pipe in self.pipes:
            pipe.scroll()
            if pipe.x_pos < -100:
                self.pipes.pop(0)

    def freeze_pipes(self):
        for pipe in self.pipes:
            pipe.draw()


# ------------------------------------------------------------------------------
# Draw numbers on the screen with a shadow centered at specified x and y point.
#
# ARGUMENTS:
# number: the number to draw
# x: point on x axis for number to be centered on.
# y: point on y axis for number to be centered on.
# ------------------------------------------------------------------------------
def draw_num(number, x, y):
    shadow = SCORE_FONT.render(str(number), False, (0, 0, 0))
    text = SCORE_FONT.render(str(number), False, (225, 255, 225))
    x = x - (text.get_width() // 2)
    y = y - (text.get_height() // 2)
    screen.blit(shadow, (x + 2, y + 2))
    screen.blit(text, (x, y))


# ------------------------------------------------------------------------------
# Store the game's current and high current.
#
# ARGUMENTS:
# bird_x: The x position of the bird
# pipe_x: The x position of the pipe
# ------------------------------------------------------------------------------
class Score:
    def __init__(self):
        self.current = 0
        self.high_score = 0

    # --------------------------------------------------------------------------
    # Draw the score at the top of the screen.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def draw_score(self):
        draw_num(self.current, WIDTH//2, 15)

    # --------------------------------------------------------------------------
    # Draw the score board in the center of the screen.
    #
    # ARGUMENTS:
    # None
    # --------------------------------------------------------------------------
    def draw_board(self):
        board = pygame.image.load(SCOREBOARD)
        board_width = board.get_width()
        board_height = board.get_height()
        x, score_y, high_score_y = WIDTH // 2, 36, 73

        screen.blit(board, ((WIDTH - board_width) // 2,
                            (HEIGHT - board_height) // 2))

        draw_num(self.current, x, score_y)
        draw_num(self.high_score, x, high_score_y)


# ------------------------------------------------------------------------------
# Start event timers. Pass in tuples with two entries (event, milliseconds)

# ARGUMENTS:
# timers: Tuples containing an event object and a time in milliseconds.
# ------------------------------------------------------------------------------
def start_timers(*timers):
    for event, milliseconds in timers:
        pygame.time.set_timer(event, milliseconds)


# ------------------------------------------------------------------------------
# Handle in-game events.
#
# ARGUMENTS:
# bird: The game's bird entity
# dummy: The game's test dummy entity
# pipe_list: The game's PipeList object
# ------------------------------------------------------------------------------
def handle_events(bird, pipe_list, score):
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                bird.jump()
                return event.key
        elif event.type == SPAWN_PIPE:
            pipe_list.spawn_pipe()
            if bird.x_pos > pipe_list.pipes[1].x_pos - 5:
                score.current += 1
                if score.current > score.high_score:
                    score.high_score = score.current


# ------------------------------------------------------------------------------
# Scroll the level from right to left.
#
# ARGUMENTS:
# background: The game's background sprite
# pipe_list: The game's PipeList object
# floor: The game's floor sprite
# ------------------------------------------------------------------------------
def scroll_level(background, pipe_list, floor):
    background.scroll()
    pipe_list.scroll_pipes()
    floor.scroll()


# ------------------------------------------------------------------------------
# Run the start screen until the user presses space bar.
#
# ARGUMENTS:
# background: The game's background sprite
# bird: The game's bird entity
# floor: The game's floor sprite
# start: The game's start screen sprite
# ------------------------------------------------------------------------------
def display_start(background, bird, floor, start):
    background.scroll()
    bird.draw()
    if bird.y_pos >= 55:
        bird.jump()
    floor.scroll()
    start.draw()


# ------------------------------------------------------------------------------
# Display score board until the user presses space bar.
#
# ARGUMENTS:
# background: The game's background sprite
# bird: The game's bird entity
# floor: The game's floor sprite
# start: The game's start screen sprite
# ------------------------------------------------------------------------------
def display_score_board(background, bird, floor, score):
    background.scroll()
    bird.draw()
    if bird.y_pos >= 55:
        bird.jump()
    floor.scroll()
    score.draw_board()


# ------------------------------------------------------------------------------
# Main function. Initialize all game assets and start the game loop.
#
# ARGUMENTS:
# None
# ------------------------------------------------------------------------------
def main():
    # Initialize event timers
    pipes = (SPAWN_PIPE, PIPE_SPAWNTIME)

    # Initialize sprites
    background = Sprite(BG_DAY, scroll_speed=0.3)
    floor = Sprite(FLOOR, y_pos=110, scroll_speed=1.25)
    start = Sprite(START, (WIDTH//2) - 25, (HEIGHT//2) - 32)
    game_over = Sprite(GAME_OVER)

    # Initialize entities
    bird = Bird(BIRD, x_pos=8, y_pos=40, weight=0.18, jump_power=12.75)

    # Initialize pipe list
    pipe_list = PipeList(PIPE_GN)

    # Initialize current
    score = Score()

    game_started = False
    game_played = False

    # Start game loop
    while True:

        # Handel events
        event = handle_events(bird, pipe_list, score)

        # Clear pipes
        pipe_list.pipes.clear()

        # Run the start screen or show the score board
        if game_played:
            display_score_board(background, bird, floor, score)
        else:
            display_start(background, bird, floor, start)

        # Start the game loop if the player presses space
        if event == pygame.K_SPACE:
            # Start the game loop
            score.current = 0
            game_started = True

            # Start event timers
            start_timers(pipes)

        while game_started:
            # Handel events
            handle_events(bird, pipe_list, score)

            # Draw background
            scroll_level(background, pipe_list, floor)

            # Draw entities
            bird.draw()
            # dummy.draw()

            # Draw current
            score.draw_score()

            # Detect collisions between bird and the four closest pipes
            if len(pipe_list.pipes) >= 4:
                for pipe in range(4):
                    pipe_entity = pipe_list.pipes[pipe]
                    # Level freezes and the bird dies if it runs into a pipe
                    if bird.detect_collision(pipe_entity):

                        # Bird is dead. The world had ended. The bird falls.
                        while bird.y_pos < bird.max_y:
                            background.freeze()
                            pipe_list.freeze_pipes()
                            floor.freeze()
                            bird.draw()
                            game_over.draw()

                            pygame.display.update()
                            game_clock.tick(FPS)

                        # Take a moment of silence for Mr. Flappy
                        sleep(1)

                        # Then move on.
                        pygame.event.clear()
                        game_played = True
                        game_started = False

            # Update display at a controlled rate
            pygame.display.update()
            game_clock.tick(FPS)

        # Update display at a controlled rate
        pygame.display.update()
        game_clock.tick(FPS)


# ------------------------------------------------------------------------------
# Entry into main loop
# ------------------------------------------------------------------------------
main()
